<div class="panel" ng-class="{'panel-primary' : context==='edit', 'panel-success' : context ==='add' }">

	<div class="panel-heading">
		<span ng-show="context==='add'">הוספת חדשה</span>
		<span ng-show="context==='edit'">עריכה:  {{item.title}}</span>
	</div>

	<div class="panel-body">
		<form name="addLink" novalidate>

			<div class="form-group" ng-class="{'has-error': addLink.title.$invalid && addLink.title.$dirty}">
				<label>כותרת</label>
				<input type="text" name="title" class="form-control" ng-model="item.title" required>
				<p class="help-block" ng-show="addLink.title.$invalid && addLink.title.$dirty">חובה להזין כותרת</p>
			</div>

			<div class="form-group" ng-class="{'has-error': addLink.url.$invalid && addLink.url.$dirty}">
				<label>קישור</label>
				<input type="url" class="form-control" name="url" ng-model="item.url" required ng-model-options="{ updateOn: 'blur' }">
				<p class="help-block" ng-show="addLink.url.$invalid && addLink.url.$dirty">יש להזין כתובת אינטרנט תיקנה</p>
			</div>

			<button type="submit" class="btn btn-success" ng-click="save()">שמירה</button>
			<button type="submit" class="btn btn-default" ng-show="context==='edit'" ng-click="add()">ביטול</button>

		</form>
	</div>

</div>
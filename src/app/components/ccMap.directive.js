(function() {

  'use strict';

  angular.module('golan')
    .directive('ccMap', ccMap)
    .directive('ccMapSmall', ccMapSmall)
  ;
    //  Google Maps Directive
    function ccMap(uiGmapGoogleMapApi, $timeout, $interval, $rootScope, AuthService) {

      var plansTaxaId   = 47126,
          insectsTaxaId = 47158,
          arcTaxaId     = 47119;

      var defaultStyles = [
        {
          featureType: 'administrative',
          elementType: 'labels.text.fill',
          stylers: [
            {
              color: '#444444'
            }
          ]
        },
        {
          featureType: 'landscape',
          elementType: 'all',
          stylers: [
            {
              color: '#f2f2f2'
            }
          ]
        },
        {
          featureType: 'poi',
          elementType: 'all',
          stylers: [
            {
              visibility: 'off'
            }
          ]
        },
        {
          featureType: 'road',
          elementType: 'all',
          stylers: [
            {
              saturation: -100
            },
            {
              lightness: 45
            }
          ]
        },
        {
          featureType: 'road.highway',
          elementType: 'all',
          stylers: [
            {
              visibility: 'simplified'
            }
          ]
        },
        {
          featureType: 'road.arterial',
          elementType: 'labels.icon',
          stylers: [
            {
              visibility: 'off'
            }
          ]
        },
        {
          featureType: 'transit',
          elementType: 'all',
          stylers: [
            {
              visibility: 'off'
            }
          ]
        },
        {
          featureType: 'water',
          elementType: 'all',
          stylers: [{
            color: '#46bcec'
          },
            {
              visibility: 'on'
            }
          ]
        },
        {
          featureType: 'water',
          elementType: 'labels.text.fill',
          stylers: [
            {visibility: 'on'},
            {color: '#444444'}
          ]
        },
        {
          featureType: 'water',
          elementType: 'labels.text.stroke',
          stylers: [
            {visibility: 'off'}
          ]
        }
      ];
      
      var defaultSetting = {
        center: {lat: 33.00603, lng: 35.48344}, // Golan Area
        zoom: 10,
        mapTypeControl: true,
        streetViewControl: false,
        styles: defaultStyles
        //mapTypeId : 'hybrid'
      };


      var gMaps,
        items,
        mapIcons,
        largeMapIcons,
        userId = null
        ;

      var markerClusterer = null,
        map = null,
        markers;

      var onMarkerClick = function (itemIndex) {
        $timeout(function () {
          $rootScope.$broadcast('activeChange', itemIndex);
        });
      };

      var itemsToMarkers = function () {
        var latLng,
          marker,
          color,
          tempItemp,
          index = 0,
          iconSet
          ;

        //  Clear All Markers
        markers = [];

        angular.forEach(items, function (item) {


          color = 'blue';
          tempItemp = {};
          if (item.latitude) {

            iconSet = mapIcons;
            if(item.ccInfo && item.ccInfo.useLargeIcon) {
              iconSet = largeMapIcons;
            }

            var iconicTaxon = null;
            if(angular.isDefined(item.iconic_taxon)){
              iconicTaxon = item.iconic_taxon.id;
            }else if (angular.isDefined(item.iconic_taxon_id)) {
              iconicTaxon = item.iconic_taxon_id;
            }
            //  Calc Item Color
            if(iconicTaxon === plansTaxaId) {              //  is this a plan
              color = 'green';
            }else if(iconicTaxon === insectsTaxaId || iconicTaxon === arcTaxaId) {      //  is this insect
              color = 'red';
            }

            tempItemp.icon = iconSet[color].normal;

            //  Locked Icon
            if (item.coordinates_obscured) {
              tempItemp.icon = iconSet[color].obscured;
            }


            latLng = new gMaps.LatLng(item.latitude, item.longitude);
            marker = new gMaps.Marker({
              position: latLng,
              icon: tempItemp.icon,
              animation: null
            });

            var itemIndex = index;
            marker.addListener('click', function () {
              onMarkerClick(itemIndex);
            });

            //  Allow Dragging?
            if(item.dragable){
              marker.setDraggable(true);
  
              marker.addListener('dragend', function (e) {
                $rootScope.$broadcast('mapItemDraged', {
                  latitude  : e.latLng.lat(),
                  longitude : e.latLng.lng()
                });
              });
            }

            this.push(marker);
            index++;
          }

        }, markers);

      };

      var setMarkerActive = function (newIndex, oldIndex) {

        if (newIndex || newIndex === 0) {

          //  Set Active Marker Style
          markers[newIndex].setAnimation(gMaps.Animation.j);
          markers[newIndex].setIcon(mapIcons['yellow'].active);

          //  Remove Active From Cluster
          markerClusterer.removeMarker(markers[newIndex]);
          markers[newIndex].setMap(map);

          //  Fix Old Active to normal
          if (oldIndex || oldIndex === 0) {

            markers[oldIndex].setAnimation(null);
            markers[oldIndex].setIcon(mapIcons['green'].normal);

            //  Add old active to Cluster
            markers[oldIndex].setMap(null);
            markerClusterer.addMarker(markers[oldIndex]);
          }
        }

        return;

      };

      var refreshMap = function () {

        if (markerClusterer) {
          markerClusterer.clearMarkers();
        }

        itemsToMarkers();
        //  Add Markers to map, use cluster if zoom is small then 9
        markerClusterer = new MarkerClusterer(map, markers, {
          //maxZoom: 10,
          gridSize: 8,
          styles: [
            {
              url: 'assets/images/cluster.png',
              height: 53,
              width: 53
            }]
        });

      };

      var initialize = function (options) {

        var margeOptions = angular.extend(defaultSetting, options);

        //margeOptions.options['MapTypeId'] = google.maps.MapTypeId
        //  Move Zome Control to TOP LEFT
        margeOptions.options['zoomControlOptions'] = {
          position: google.maps.ControlPosition.TOP_LEFT
        };
        map = new gMaps.Map(document.getElementById('map'), margeOptions);

        //  Set Maps Markers
        largeMapIcons = {
          green: {
            active: {
              url: 'assets/images/iconsprite-large.png',
              size: new google.maps.Size(45, 54),
              origin: new google.maps.Point(0, 0)
            },
            normal: {
              url: 'assets/images/iconsprite-large.png',
              size: new google.maps.Size(30, 45),
              origin: new google.maps.Point(40, 0)
            },
            obscured: {
              url: 'assets/images/iconsprite-large.png',
              size: new google.maps.Size(25, 25),
              origin: new google.maps.Point(72, 0)
            }
          },
          blue: {
            active: new gMaps.MarkerImage('assets/images/iconsprite-large.png', new gMaps.Size(34, 44), new gMaps.Point(0, 45)),
            normal: {
              url: 'assets/images/iconsprite-large.png',
              size: new google.maps.Size(30, 45),
              origin: new google.maps.Point(40, 50)
            },
            obscured: {
              url: 'assets/images/iconsprite-large.png',
              size: new google.maps.Size(25, 25),
              origin: new google.maps.Point(72, 52)
            }
          },
          grey: {
            active: new gMaps.MarkerImage('assets/images/iconsprite-large.png', new gMaps.Size(34, 42), new gMaps.Point(0, 88)),
            normal: new gMaps.MarkerImage('assets/images/iconsprite-large.png', new gMaps.Size(23, 37), new gMaps.Point(36, 88)),
            obscured: new gMaps.MarkerImage('assets/images/iconsprite-large.png', new gMaps.Size(21, 23), new gMaps.Point(60, 88))
          },
          yellow: {
            active: new gMaps.MarkerImage('assets/images/iconsprite-large.png', new gMaps.Size(34, 42), new gMaps.Point(0, 131)),
            normal: new gMaps.MarkerImage('assets/images/iconsprite-large.png', new gMaps.Size(23, 37), new gMaps.Point(36, 131)),
            obscured: new gMaps.MarkerImage('assets/images/iconsprite-large.png', new gMaps.Size(21, 23), new gMaps.Point(60, 131))
          },
          red: {
            active: new gMaps.MarkerImage('assets/images/iconsprite-large.png', new gMaps.Size(34, 42), new gMaps.Point(0, 150)),
            normal: new gMaps.MarkerImage('assets/images/iconsprite-large.png', new gMaps.Size(27, 42), new gMaps.Point(40, 203)),
            obscured: new gMaps.MarkerImage('assets/images/iconsprite-large.png', new gMaps.Size(26, 25), new gMaps.Point(69, 203))
          }
        };

        mapIcons = {
          green: {
            active: new gMaps.MarkerImage('assets/images/iconsprite.png', new gMaps.Size(34, 44), new gMaps.Point(0, 0)),
            normal: new gMaps.MarkerImage('assets/images/iconsprite.png', new gMaps.Size(23, 37), new gMaps.Point(36, 0)),
            obscured: new gMaps.MarkerImage('assets/images/iconsprite.png', new gMaps.Size(21, 23), new gMaps.Point(60, 0))
          },
          blue: {
            active: new gMaps.MarkerImage('assets/images/iconsprite.png', new gMaps.Size(34, 44), new gMaps.Point(0, 45)),
            normal: new gMaps.MarkerImage('assets/images/iconsprite.png', new gMaps.Size(23, 37), new gMaps.Point(36, 45)),
            obscured: new gMaps.MarkerImage('assets/images/iconsprite.png', new gMaps.Size(21, 23), new gMaps.Point(60, 45))
          },
          grey: {
            active: new gMaps.MarkerImage('assets/images/iconsprite.png', new gMaps.Size(34, 42), new gMaps.Point(0, 88)),
            normal: new gMaps.MarkerImage('assets/images/iconsprite.png', new gMaps.Size(23, 37), new gMaps.Point(36, 88)),
            obscured: new gMaps.MarkerImage('assets/images/iconsprite.png', new gMaps.Size(21, 23), new gMaps.Point(60, 88))
          },
          yellow: {
            active: new gMaps.MarkerImage('assets/images/iconsprite.png', new gMaps.Size(34, 42), new gMaps.Point(0, 131)),
            normal: new gMaps.MarkerImage('assets/images/iconsprite.png', new gMaps.Size(23, 37), new gMaps.Point(36, 131)),
            obscured: new gMaps.MarkerImage('assets/images/iconsprite.png', new gMaps.Size(21, 23), new gMaps.Point(60, 131))
          },
          red: {
            active: new gMaps.MarkerImage('assets/images/iconsprite.png', new gMaps.Size(34, 42), new gMaps.Point(0, 177)),
            normal: new gMaps.MarkerImage('assets/images/iconsprite.png', new gMaps.Size(23, 37), new gMaps.Point(36, 177)),
            obscured: new gMaps.MarkerImage('assets/images/iconsprite.png', new gMaps.Size(21, 23), new gMaps.Point(60, 177))
          }
        };

        //  Update Parent When ZOOM Changed
        var holdZoom = map.getZoom();
        map.addListener('zoom_changed', function () {
          $rootScope.$broadcast('zoomChange', (map.getZoom() - holdZoom)); // Just Send The Direction of the zooming
          holdZoom = map.getZoom();
        });

        //  Update Parent When CENTER Changed
        map.addListener('center_changed', function () {
          $rootScope.$broadcast('centerChange', map.getCenter());
        });

        map.addListener('maptypeid_changed', function(){

          if(this.getMapTypeId() === 'hybrid'){
            this.setOptions({styles : []});
          }else {
            this.setOptions({styles : defaultStyles});
          }
          
        });

      };

      var updateMapOptions = function (options) {
        // console.log('Updating: ',options);
        //  For Now, Only updating center,

        if (options && options.center) {
          map.setCenter(new gMaps.LatLng(
            Number(options.center.lat),
            Number(options.center.lng)
          ));
        }


      };

      var link = function ($scope) {
        $scope.mapsReady = false;

        //  console.log('Linking Directive');
        uiGmapGoogleMapApi.then(function (maps) {
          gMaps = maps;

          //  Get User info before initialzing,
          AuthService.getUserInfo().then(
            function (userinfo) {
              userId = userinfo.id;
              initialize($scope.options);
            },
            function () {
              initialize($scope.options);
            });

        });

        $scope.$watch('items', function () {
          if ($scope.items) {

            //  Clear Active From map
            if ($scope.active && markers[$scope.active]) {
              markers[$scope.active].setMap(null);
            }

            items = angular.isArray($scope.items) ? $scope.items : [$scope.items];

            if (!gMaps) {
              //  Timer Wait For Google Maps To load
              var readyTimer = $interval(function () {
                if (gMaps) {
                  refreshMap();
                  $interval.cancel(readyTimer);
                }
              }, 250, 10);
            } else {
              refreshMap();
            }
          }
        });

        $scope.$watch('options', function (newOpt) {
          if (gMaps) {
            updateMapOptions(newOpt);
          }
        }, true);

        $scope.$watch('active', setMarkerActive);

        //  Small Map Change Center
        $scope.$on('smallCenterChange', function (e, center) {
          map.setCenter(center);
        });

      };

      return {
        scope: {
          items: '=',
          options: '=',
          active: '='
        },
        template: '<div id="map"></div>',
        link: link
      };
    }

    function ccMapSmall(uiGmapGoogleMapApi, $rootScope) {

      var gMaps,
        map,
        currentZoom,
        centerListner
        ;

      // Send Center Changing event only if The event wasnt trigger from the large map
      //  This will prevent endless loop
      var toggleCenterListner = function (map, toggle) {
        if (toggle) {
          centerListner = map.addListener('center_changed', function () {
            $rootScope.$broadcast('smallCenterChange', map.getCenter());
          });
        } else {
          google.maps.event.removeListener(centerListner);
        }
      };

      var initialize = function () {
        map = new gMaps.Map(document.getElementById('small-map'), {
          center: {lat: 33.00603, lng: 35.48344}, // Golan Area
          zoom: currentZoom,
          disableDefaultUI: true,
          mapTypeIds: gMaps.MapTypeId.TERRAIN,
          styles: [
            {
              featureType: 'road',
              stylers: [
                {visibility: 'off'}
              ]
            }, {
              featureType: 'landscape',
              stylers: [
                {visibility: 'off'}
              ]
            }, {
              featureType: 'poi',
              stylers: [
                {visibility: 'off'}
              ]
            }, {
              featureType: 'administrative',
              stylers: [
                {visibility: 'off'}
              ]
            }, {
              featureType: 'administrative.country',
              elementType: 'geometry',
              stylers: [
                {visibility: 'on'}
              ]
            }, {
              featureType: 'landscape.natural',
              elementType: 'geometry.fill',
              stylers: [
                {visibility: 'on'}
              ]
            }
          ]
        });

        toggleCenterListner(map, true);
      };

      var link = function ($scope) {

        uiGmapGoogleMapApi.then(function (maps) {
          currentZoom = 7;
          gMaps = maps;
          initialize();
        });

        $scope.$on('zoomChange', function (e, changeDirection) {
          if (map) {
            currentZoom = currentZoom + changeDirection;

            if (currentZoom > 4) {
              toggleCenterListner(map, false);
              map.setZoom(currentZoom);
              toggleCenterListner(map, true);
            }
          }
        });
        $scope.$on('centerChange', function (e, center) {
          if (map) {

            if (currentZoom > 4) {
              toggleCenterListner(map, false);
              map.setCenter(center);
              toggleCenterListner(map, true);
            }
          }
        });
      };

      return {
        scope: {},
        template: '<div id="small-map"></div>',
        link: link
      };
    }

  
})();
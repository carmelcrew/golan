<?php

$intArr = array(
	'urls'  => array(
		'list'  => $this->Html->Url('/json/curators', true),
		'del'   => $this->Html->Url('del', true),
		'add'   => $this->Html->Url('add', true)
	)
);

$this->Js->set('golanBackEnd', $intArr);
echo $this->Js->writeBuffer(array('onDomReady' => false));

?>
<div class="curator" ng-controller="CuratorsCtrl">
	<div class="page-header">
		<h3>רשימת אוצרים</h3>
	</div>

	<div class="row">

		<div class="col-md-6">

			<div class="panel panel-primary cc-list">
				<div class="panel-heading">רשימת אוצרים</div>

				<div class="panel-body">

					<div class="list-group">
						<div class="list-group-item" ng-repeat="item in items" ng-class="{active: item.id === activeItem.id}">
							<h4 class="list-group-item-heading">
								{{item.login}}
							</h4>
							<div class="pull-left actions">
								<i class="fa fa-times pointer"  title="הסר אוצר" ng-click="del(item)"></i>
							</div>
						</div>
					</div>

					<div class="alert alert-warning" ng-show="!items.length">לא מוגדרים אוצרים במערכת</div>
				</div>
			</div>

		</div><!-- col-6 -->

		<div class="col-md-6">
			<cc-add-edit-curator item="activeItem"></cc-add-edit-curator>
		</div>

	</div><!-- row -->
</div>


<script type="text/ng-template" id="confirmDelModal.html">
	<div class="modal-header">
		<h5 class="modal-title">
			<span>האם למחוק את</span>
			{{item.login}}
		</h5>
	</div>
	<div class="modal-footer tal">
		<button class="btn btn-default" type="button" ng-click="cancel()">ביטול</button>
		<button class="btn btn-primary" type="button" ng-click="ok()">מחק</button>
	</div>
</script>
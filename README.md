# Golan Wildlife Tracking using Social Monitoring #

* The Golan project is an implementation of Hebrew front-end + extra functionality on top of [iNaturalist](http://www.inaturalist.org)
* Production project is running at: http://tatzpiteva.org.il
* This solution was developed by http://carmel.coop - you're welcome to contact us with any question. (through here, or email).
* Automated testing is graciously provided by [BrowserStack](https://www.browserstack.com/)

## License
This software is licensed under [MIT License](https://opensource.org/licenses/MIT)

## Tech Stuff ##
* PHP, CakePHP 2.x
* MySQL server
* Angular
* Bootstrap
* iNaturalist global server as back end
* We support our own customised variation of the iNat mobile apps
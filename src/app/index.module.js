(function() {
  'use strict';

  angular
    .module('golan', [    
      'ngAnimate',
      'ngCookies',
      'ngTouch',
      'ngSanitize',
      //'ngResource',
      'ui.router',
      'ui.bootstrap',
      'slick',
      'uiGmapgoogle-maps',
      'perfect_scrollbar',
      'angular-loading-bar',
      'ui-notification',
      'flow',
      'httpPostFix',
      'ngPromiseExtras'
    ]);

})();

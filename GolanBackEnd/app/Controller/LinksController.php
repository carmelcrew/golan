<?php
App::uses('AppController', 'Controller');

class LinksController extends AppController {

	// public $components = array('Session');

	// Admin Functions

	// Admin index
	public function control_index()
	{
		$this->headerMenu['links'] = 'active';
		$this->Link->recursive = -1;
		$links_all = $this->Link->find('all');
		$this->set('links', $links_all);
		$this->set('title_for_layout', 'ניהול קישורים');
	}

	//  Add Links
	function control_add(){

		$this->layout = false;
		$data = $this->request->data;
		//pr($data);



		if ($this->request->is('post')) {
			$this->autoRender = false;

			//  Updating?
			if(isset($this->request->data['id']))
				$this->Link->id = $this->request->data['id'];
			else //Creating
				$this->Link->create();


			if($this->Link->save($data)){
				return json_encode(array('success'=> true));
			}else {
				return json_encode(array('success'=> false));
			}
		}
	}
	//  Delete A Links
	function control_del($id = null) {
		$this->autoRender = false;

		if($id) {
			$this->Link->delete($id);
		}

		$this->redirect('index');
	}


	// JSON API functions

	// Links index
	function json_index($encode = true)
	{
		$this->Link->recursive = -1;
		$links = $this->Link->find('all');
		// put them all in an anon array
		$ret = array();
		foreach( $links as $L )
			$ret[] = $L['Link'];
		// return json array of Links
		return $encode ? json_encode($ret) : $ret;
	}
}

(function() {
  'use strict';

  angular
    .module('golan')
    .config(routerConfig)
  ;

  
  function routerConfig($stateProvider, $urlRouterProvider, $locationProvider) {

    var pages = 'app/pages/';

    $stateProvider
      .state('root', {
        abstract: true,
        template: '<div ui-view=""></div>',
        resolve: {
          globalData: function($q, $http, Globals){
          
            var globalData = {};
            var deferred = $q.defer();
          
            //  Is This A cakephp Load? (Globals allready loaded)
            if(typeof app === 'object' && app.golanBackEnd) {
            
              angular.extend(globalData, app.golanBackEnd);
              Globals.set(globalData);
              deferred.resolve(globalData);
            
            }
            else {  //  Loading Outside of cake, Need To get globals
              $http.get('http://localhost/golan/GolanBackEnd/json/app/init', {cache: true}).success(function(data) {
                angular.extend(globalData,data);
                Globals.set(globalData);
                deferred.resolve(globalData);
              });
            }

            return deferred.promise;
          },
        
          //  globalData is used here just to make sure globals are set before checking token exists
          userInfo: function ($q, AuthService, globalData) {

            AuthService.onLoadStatus().then(
              function(data){
                deferred.resolve(data);
              },
              function(){ //  User Not Logged in
                deferred.resolve(false);
              }
            );

            var deferred = $q.defer();

            return deferred.promise;
          }
        }
      })

      .state('home', {
        parent: 'root',
        url: '/',
        params: {filter: null},
        templateUrl: pages + 'home/home.html',
        controller: 'HomeCtrl',
        onEnter: function(){
          angular.element('body').addClass('no-scroll');
        },
        onExit: function(){
          angular.element('body').removeClass('no-scroll');
        }
        
      })
    
      .state('about', {
        parent: 'root',
        url: '/about',
        templateUrl: pages + 'about/about.html',
        controller: 'AboutCtrl'
      })
    
      //  See Obs inner routing at obs.js
      .state('obs', {
        parent: 'root',
        url: '/obs/',
        abstract: true,
        template: '<div ui-view=""></div>'
      })
    
      .state('user', {
        parent: 'root',
        url: '/users/view/:slug',
        controller: 'UsersController',
        abstract: true,
        templateUrl: pages + 'users/user.html'
      })
    
      //  See Projects inner routing at projects.js
      .state('projects', {
        parent: 'root',
        url: '/projects',
        abstract: true,
        template: '<div ui-view=""></div>'
      })
    
      .state('activity', {
        parent: 'root',
        url: '/activity',
        templateUrl: pages + 'activity/activity.html',
        controller: 'ActivityController'
      })
    
      .state('join', {
        parent: 'root',
        url: '/join',
        templateUrl: pages + 'join/join.html',
        controller: function($scope, restApi){
          restApi.cake.texts('join').then(function(text){
            $scope.joinText = text;
          });
        }
      })
    
      .state('contact', {
        parent: 'root',
        url: '/contact',
        templateUrl: pages + 'contact/contact.html',
        controller: 'ContactController'
      })
    
      //  See Projects inner routing at pages/news/news.js
      .state('news', {
        parent: 'root',
        url: '/news',
        abstract: true,
        template: '<div ui-view=""></div>'
      })
    
      .state('info', {
        parent: 'root',
        url: '/info',
        templateUrl: pages + 'info/articles.html',
        controller: 'ArticlesController'
      })
    
      .state('login', {
        parent: 'root',
        url: '/login/',
        templateUrl: pages + 'login/login.html',
        controller: 'loginController',
        params: {
          redirect: null,
          notify: null
        }
      })
    
      .state('register', {
        parent: 'root',
        url: '/register',
        templateUrl: pages + 'register/register.html'
      })
  
    ;

    //  If 'stats' let cake handle routing
    $urlRouterProvider.when(/stats/i, function(){
      return true;
    });

    $urlRouterProvider.otherwise('/');
  
    $locationProvider.html5Mode(true);
    //   enabled: true,
    //   requireBase: false
    // });
  }

})();

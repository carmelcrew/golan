<?php
// pr($projects);
//$searchProjectUrl = $this->Html->url('search');

$intArr = array(
	'urls'  => array(
		'list'  => $this->Html->Url('/json/news', true),
		'del'   => $this->Html->Url('del', true),
		'add'   => $this->Html->Url('add', true)
	)
);

$this->Js->set('golanBackEnd', $intArr);
echo $this->Js->writeBuffer(array('onDomReady' => false));

?>
<div class="news" ng-controller="newsCtrl" ng-init='init(<?php echo json_encode($intArr); ?>)'>
	<div class="page-header">
		<h3>רשימת חדשות</h3>
	</div>

	<div class="row">

		<div class="col-md-6">

			<div class="panel panel-primary cc-list">
				<div class="panel-heading">רשימת חדשות</div>

				<div class="panel-body">

					<div class="list-group">
						<div class="list-group-item" ng-repeat="item in items" ng-class="{active: item.id === activeItem.id}">
							<h4 class="list-group-item-heading">
								{{item.headline}}
							</h4>
							<div class="pull-left actions">
								<i class="fa fa-times pointer" title="מחק חדשה" ng-click="del(item)"></i>
								<i class="fa fa-pencil pointer" title="ערוך חדשה" ng-click="edit(item)"></i>

							</div>
							<small>{{item.updated_at | badDateToISO |date:'shortDate'}}</small>
						</div>
					</div>

					<div class="alert alert-warning" ng-show="!items.length">לא נמצאו חדשות</div>
				</div>
			</div>

		</div><!-- col-6 -->

		<div class="col-md-6">
			<cc-add-edit item="activeItem"></cc-add-edit>
		</div>

	</div><!-- row -->
</div>


<script type="text/ng-template" id="confirmDelModal.html">
	<div class="modal-header">
		<h5 class="modal-title">
			<span>האם למחוק את</span>
			{{item.headline}}
		</h5>
	</div>
	<div class="modal-footer tal">
		<button class="btn btn-default" type="button" ng-click="cancel()">ביטול</button>
		<button class="btn btn-primary" type="button" ng-click="ok()">מחק</button>
	</div>
</script>
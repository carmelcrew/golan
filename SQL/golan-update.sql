-- Sequential SQL update file

-- Projects displayed
CREATE TABLE IF NOT EXISTS `projects` (
	`id` INT UNSIGNED NOT NULL,
	`title` TINYTEXT NOT NULL,
	`slug` TINYTEXT NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

-- Gallery pics
CREATE TABLE IF NOT EXISTS `pics` (
	`id` INT UNSIGNED NOT NULL,
	`name` TINYTEXT NOT NULL,
	`url` TINYTEXT NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

INSERT INTO `pics` (`id`, `name`, `url`) VALUES ('2319617', 'Wild Pig', 'http://static.inaturalist.org/photos/2580193/medium.?1446042265');
INSERT INTO `pics` (`id`, `name`, `url`) VALUES ('2100034', 'Common Tortoise', 'http://static.inaturalist.org/photos/2514886/medium.?1444647976');
INSERT INTO `pics` (`id`, `name`, `url`) VALUES ('1884379', 'Romulea', 'http://static.inaturalist.org/photos/2296958/medium.JPG?1440313511');

-- Add oreder to pics - 08.11.2015
ALTER TABLE `pics`
	ADD COLUMN `order` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0 AFTER `url`;

-- Alpha-1 go live 2015-11-23

-- Add news table
CREATE TABLE `news` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`headline` VARCHAR(255) NOT NULL,
	`content` TEXT NULL,
	`updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci' CHARSET=utf8
ENGINE=InnoDB;

-- Add links table
CREATE TABLE `links` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`url` TEXT NOT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci' CHARSET=utf8
ENGINE=InnoDB;

-- Add articles and categories
CREATE TABLE `articles` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`subject` VARCHAR(255) NOT NULL,
	`content` TEXT NOT NULL,
	`author_id` VARCHAR(50) NOT NULL,
	`cat_id` INT(10) UNSIGNED NULL DEFAULT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci' CHARSET=utf8
ENGINE=InnoDB;

CREATE TABLE `categories` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci' CHARSET=utf8
ENGINE=InnoDB;

-- Add Title To links table
ALTER TABLE `links`
	ADD COLUMN `title` TEXT NULL AFTER `created_at`;

--	Add Curators table
CREATE TABLE `curators` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`login` TINYTEXT NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

-- The below should be selectively applied, only on DBs that have art_cat table
-- DROP TABLE articles_categories
-- ALTER TABLE articles ADD COLUMN `cat_id` INT UNSIGNED NULL DEFAULT NULL AFTER `author_id`;

-- things after beta-1 go live 2015.12.24

ALTER TABLE `articles`
	CHANGE COLUMN `author_id` `author` VARCHAR(50) NOT NULL AFTER `content`,
	ADD COLUMN `file` VARCHAR(255) NULL AFTER `cat_id`;

ALTER TABLE `projects`
	ADD COLUMN `menu_flag` TINYINT UNSIGNED NULL DEFAULT '0' AFTER `slug`,
	ADD COLUMN `smart_flag` TINYINT UNSIGNED NULL DEFAULT '0' AFTER `menu_flag`;

CREATE TABLE `texts` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`slug` VARCHAR(50) NULL,
	`content` LONGTEXT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
 
-- Add Publish Date to articles
ALTER TABLE `articles`
	ADD COLUMN `published_at` TIMESTAMP NULL AFTER `created_at`;
	
ALTER TABLE `articles`
	CHANGE COLUMN `published_at` `published` TIMESTAMP NULL DEFAULT NULL AFTER `created_at`,
	ADD COLUMN `published_at` TINYTEXT NULL DEFAULT NULL AFTER `published`;
	
ALTER TABLE `projects`
	ADD COLUMN `icon_url` TINYTEXT NOT NULL AFTER `slug`,
	ADD COLUMN `description` TINYTEXT NOT NULL AFTER `icon_url`,
	ADD COLUMN `observations_count` SMALLINT NULL AFTER `description`,
	ADD COLUMN `taxa_count` SMALLINT NULL AFTER `observations_count`;

INSERT INTO `texts` (`id`,`slug`,`content`) VALUES (1,'about','<p>&#34;תצפיטבע&#34; - מיזם קהילתי, המשותף למועצה האזורית גולן, המכון לחקר הגולן ואוניברסיטת חיפה, ועיקרו, שיתוף הקהילה בניטור רציף של הטבע הגולני.<br/><br/>מתוך ההכרה <b>בחשיבות תיפקודן של המערכות האקולוגיות בגולן</b>, וכהמשך ישיר לתוכנית האב לשטחים הפתוחים בגולן עלה הצורך לעקוב ולנטר את מצב הטבע באזור.היישומון תצפיטבע פותח על בסיס הידע שנצבר במקומות שונים בעולם, . ומאפשר לכל אחד מתושבי הגולן, ובהמשך, לכל מטייל באשר הוא, להשתתף באופן פעיל במעקב רציף אחר הטבע הגולני. בעזרת תצפיטבע נוכל לעקוב אחר צמחים, בעלי חיים ומקורות מים, תוך שותפות בין אנשי מקצוע לבין קהילת הגולן. תיעוד הטבע הגולני ע&#34;י תושבי הגולן ישמש את החוקרים להבנה טובה יותר של השינויים החלים במערכות <a href=\"http://google.com\" target=\"\">האקולוגיות</a>, ויהווה בסיס ידע לקבלת החלטות של קובעי המדיניות באזור.<br/><br/>המשתמשים ביישומון יוכלו, בכל זמן נתון, לתעד צמחים ובעלי חיים ולהעבירם אל מאגר הנתונים של צוות המיזם. במקביל לתהליך זה יתקיים היזון חוזר ומשוב אל המנטרים, (קהילת הגולן) ויועבר מידע רלבנטי העוסק בנושאי טבע וסביבה באופן כללי.<br/><br/>המידע והעדכון השוטף באתר תצפיטבע נועד <u>לשימושם</u> של תלמידים, סטודנטים, <i>חוקרים, וכל מי שיש לו עניין בשימור הטבע הגולני.</i><br/><br/>המיזם &#34;תצפיטבע&#34;, ישמש בעתיד את כלל תושבי ארצנו, בכל אזורי הנופש והטיולים הפרושים לרוחבה ולאורכה של ארצנו. תיעוד הטבע ע&#34;י הקהילה , יהדק את הקשר שבין התושבים לבין עצמם ובין התושבים לטבע שסביבם, תוך שמירה על המערכות האקולוגיות והסביבה שבה אנחנו חיים, ויבטיח איכות חיים לכולנו.</p>');
INSERT INTO `texts` (`id`,`slug`,`content`) VALUES (2,'activity','<p>שלום וברוכים הבאים לתצפיטבע.<br/>אנו נמצאים בעיצומו של שלב בטא בפיתוח היישומון (אפליקציה) והאתר הנלווה.<br/>קבוצות מתנדבים מקרב תושבי הגולן, נוטלים חלק בהתנסות ראשונית, שמאפשרת למפתחים לשדרג את יכולות האתר והיישומון.<br/>במקביל מתקיימת עבודה רבה ליצירת קשר בלתי אמצעי בתחום הקהילתי – שיתוף תושבי הגולן, פעילות במערכות החינוך הפורמלי והבלתי פורמלי.<br/>חוקרים מתחומי האקולוגיה, בוטניקה וזואלוגיה, בוחנים אזורים לניטור ומושאי ניטור, שיענו על שאלות מחקריות העולות כרגע בהקשר לשמירת הטבע הגולני.<br/><br/></p>');
INSERT INTO `texts` (`id`,`slug`,`content`) VALUES (3,'contact','<p>ניתן ליצור איתנו קשר בטופס מטה. נשמח לשמוע מכם בכל עניין.</p>');
INSERT INTO `texts` (`id`,`slug`,`content`) VALUES (4,'links','להלן רשימה קצרה של קישורים \\ מאמרים שכדאי לקרוא.');


ALTER TABLE `news`
	ADD COLUMN `short` TEXT NULL AFTER `headline`;	

INSERT INTO `texts` (`id`,`slug`,`content`) VALUES (5,'about_bottom','<p>קחו חלק בקהילה המתרחבת של מתנדבים המנטרים את הטבע הגולני.<br/></p>');
INSERT INTO `texts` (`id`,`slug`,`content`) VALUES (6,'join','<p><br/>מטרת האפליקציה והאתר היא לאפשר לכל אדם באשר הוא -בטיול, פיקניק או כל פעילות אחרת בטבע, לצלם ,לתעד ולהעלות תצפיות של בעלי חיים ,צמחים ובתי גידול לחים בגולן.<br/>ניתן להעלות תצפית ישירות בשטח - דרך הפלאפון ( אנדרואיד ואיפון ).<br/><br/>בנוסף האתר והאפליקציה מאפשרים להעלות את התצפית, גם לאחר היציאה מהשטח, דרך המכשיר הסלולרי או אתר האינטרנט.<br/>בעזרת היישומון תוכלו לזהות את מין החיה או הצמח, לעזור לאחרים לזהות תצפיות שלהם, לכתוב הערות על תצפיות מעניינות, ללמוד על החי וצומח של הגולן ובכך לתרום לפרויקט ייחודי מסוגו של ניטור הטבע הגולני.<br/>צילום בהיר יאפשר זיהוי קל יותר, של בעל החיים או הצמח. מומלץ במקרים של צמחים וחרקים לצלם כמה תמונות מכמה זוויות ( בכל תצפית ניתן להעלות מספר צילומים, מומלץ עד 3-4 ).<br/><br/>בזמן העלאת תצפית קיימת אפשרות להסתיר את המיקום המדויק על מנת לא לפגוע בערכי טבע או מקומות רגישים. כמו כן קיימת אפשרות להעלות תצפיות של חיות דרוסות.<br/><br/>האפליקציה ידידותית וקלה לשימוש:<br/><br/>פתיחה, עמוד ראשי. לחיצה על אייקון המצלמה. צילום (או מספר צילומים).<br/><br/>מעבר לדף תצפית. השלמת פרטים: זיהוי, הערות, מוסתר ,חי או מת וכו...<br/><br/>העלאת תצפית למערכת. (אין צורך בהתנתקות כל פעם ניתן להישאר מחוברים וזמינים לתצפית הבאה.)<br/><br/>בכל כניסה יש למשוך את המסך כלפי מטה לסנכרון תצפיות קודמות.<br/><br/>&#34;במצב מפה&#34; באפליקציה ניתן לראות תצפיות שלי, של אחרים, לבצע זיהוי ולהתעדכן על מה שקורה בסביבה.<br/><br/>אתר תצפיטבע מאפשר בנוסף להעלאת תצפיות, חיבור אל תצפיות של משתמשים אחרים, אפשרות להשתתף בזיהוי ודיון על התצפיות, חשיפה למאמרים מקצועיים, חיפושים מתקדמים של מינים , חיפוש לפי משתמשים, הודעות מצוות הפרויקט , קישורים למגדירים ומקורות מידע אחרים ועוד...<br/><br/>בכל בעיה או שאלה ניתן לפנות לצוות הפרויקט .<br/></p>');

-- Go live 2016-03-24

CREATE TABLE `users` (
	`id` INT UNSIGNED NOT NULL,
	`login` TINYTEXT NOT NULL,
	`name` TINYTEXT NULL DEFAULT NULL,
	`icon` TINYTEXT NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci' ENGINE=InnoDB;


ALTER TABLE `projects`
	ADD COLUMN `members_count` SMALLINT(6) NULL DEFAULT NULL AFTER `taxa_count`;

-- Go live 2016-05-03

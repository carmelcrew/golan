(function() {
'use strict';

  angular.module('golan')
    .controller('ProfileCtrl', ctrl)
  ;

  function ctrl($scope, $window, AuthService, Globals, $state, userInfo, Notification, $rootScope) {

    var globals = Globals.get();

    var init = function(data){
      if(!data){
        //  Not Singin
        return false;
      }
      $scope.userInfo = data;
      setFormFields(data);
      $scope.scopeLoading = true;
    };

    var setFormFields = function(data){
      $scope.formFields = {
        name:         data.name,
        description:  data.description,
        email:        data.email,
        login:        data.login
      };
    };

    $scope.projects     = globals.projects;
    $scope.userProjects = [];
    $scope.userLoggedIn = false;
    $scope.userInfo     = {};
    $scope.scopeLoading = false;
    $scope.loading      = false;
    $scope.success      = false;

    $scope.uploader = {};

    $scope.saveProfile = function(){

      //  Manualy make sure all fileds are checked
      angular.forEach($scope.profileEdit.$error.required, function(field) {
        field.$setDirty();
      });

      if($scope.profileEdit.$valid && !$scope.profileEdit.$untouched){

        //  Get Only fields that was changed by user
        var changedFields = {};
        angular.forEach($scope.formFields, function(field,key){
          if($scope.profileEdit[key] && !$scope.profileEdit[key].$pristine && $scope.profileEdit[key].$touched){
            this['user[' + key + ']'] = field;
          }
        },changedFields);

        //console.log('All Valid', changedFields);
        //  File Logic
        if($scope.uploader.flow && $scope.uploader.flow.files.length){
          //  We Have a file
          changedFields.file = $scope.uploader.flow.files[0].file;
        }
        

        //  Any Fields changed?
        if(!angular.equals({}, changedFields)){
          //  Do Saving
          $scope.loading = true;

          AuthService.editUser($scope.userInfo.login, changedFields).then(
            function(){
              $scope.loading = false;
              $scope.success = true;
              Notification.success('פרופיל נשמר בהצלחה');

              $rootScope.$broadcast('loginChanged', true);
              $state.go('user.obs');
            },
            function(){
              $scope.loading = false;
              Notification.error('השמירה נכשלה');
            }

          );
        }else {
          $scope.loading = false;
        }
      }

      return;

    };

    //  Do Init
    init(userInfo);
    
    $scope.$on('loginChanged', function(event, status){
      //console.log('Login Changed',status);
      if(status) {
        AuthService.getUserInfo().then(function(data){
          $scope.userInfo = data;
        });
      }else {
        $state.go('user.obs');
      }
    });

  }

})();
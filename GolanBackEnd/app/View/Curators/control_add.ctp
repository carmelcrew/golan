<div class="panel panel-success">

	<div class="panel-heading">
		<span>הוספת אוצר</span>
	</div>

	<div class="panel-body">
		<form name="addLink" novalidate>

			<div class="form-group">
				<label>מזהה אוצר</label>
				<input type="text" name="headline" class="form-control" ng-model="item.login" placeholder="לדוגמא: ariel-shamir">
			</div>
			<button type="submit" class="btn btn-success" ng-click="save()">שמירה</button>

		</form>
	</div>

</div>
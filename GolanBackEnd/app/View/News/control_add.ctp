<div class="panel" ng-class="{'panel-primary' : context==='edit', 'panel-success' : context ==='add' }">

	<div class="panel-heading">
		<span ng-show="context==='add'">הוספת חדשה</span>
		<span ng-show="context==='edit'">עריכה:  {{item.headline}}</span>
	</div>

	<div class="panel-body">
		<form>
			<div class="form-group">
				<label>כותרת</label>
				<input type="text" name="headline" class="form-control" ng-model="item.headline">
			</div>

			<div class="form-group">
				<label>תקציר</label>
				<br>
				<small class="text-muted">התקציר מופיע בעמוד העדכונים</small>
				<div text-angular ng-model="item.short"></div>
			</div>

			<div class="form-group">
				<label>תוכן</label>
				<br>
				<small class="text-muted">התוכן יופיע בעמוד החדשה בלבד</small>
				<div text-angular ng-model="item.content"></div>
			</div>
			<button type="submit" class="btn btn-success" ng-click="save()">שמירה</button>
			<button type="submit" class="btn btn-default" ng-show="context==='edit'" ng-click="add()">ביטול</button>
		</form>
	</div>

</div>
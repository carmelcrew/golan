<?php
$intArr = array(
	'picsJson'	=> $this->Html->url('/json/pics'),
	'del'	    => $this->Html->url('del'),
	'edit'	    => $this->Html->url('edit'),
	'reOrder'   => $this->Html->url('save_order'),
	'add'       => $this->Html->url('add')
);
?>

<div class="pics" ng-controller="picsCtrl" ng-init='init(<?php echo json_encode($intArr); ?>)'>
	<div class="page-header">
		<h3>גלריית תמונות - דף אודות</h3>
		<span class="text-muted">הגלריה בדף אודות מורכבת מתצפיות שהמנהל מגדיר בעמוד זה</span>
	</div>
	<div class="row">

		<div class="col-md-6">
			<div class="panel panel-primary cc-list">

				<div class="panel-heading">פריטים בגלריה<i class="fa fa-spinner fa-spin" ng-show="loading"></i>

				</div>

				<ul dnd-list="items" class="media-list">
					<li ng-repeat="item in items"
						dnd-draggable="item"
						dnd-effect-allowed="move"
						dnd-moved="reOrder($index)"
						dnd-selected="models.selected = item"
						ng-class="{'selected': models.selected === item}"
						class="media"
					>
						<div class="media-left">
							<a href="#">
								<img class="media-object" ng-src="{{item.url}}">
							</a>
						</div>
						<div class="media-body">
							<h4 class="media-heading">{{item.name}}</h4>
							{{item.id}}

							<div class="actions">
								<div class="btn-group-vertical" role="group" aria-label="...">
									<button type="button" class="btn btn-default" ng-click="edit(item)"><span class="fa fa-pencil-square-o"></span></button>
									<button type="button" class="btn btn-default" ng-click="del(item)"><span class="fa fa-times"></span></button>
								</div>
							</div>
						</div>
					</li>
				</ul>

			</div>
		</div>

		<div class="col-md-6" ng-controller="AddPicCtrl">
			<div class="panel panel-primary add-pic">
				<div class="panel-heading">הוספת פריטים לגלריה</div>
				<div class="panel-body">
					<label>חיפוש אייטמים<i class="fa fa-spinner fa-spin" ng-show="loading"></i></label>
					<div class="input-group">
						<input type="text" ng-model="query" cc-enter="search()" class="form-control" placeholder="חפש לפי שם או מזהה">
				        <span class="input-group-btn">
				            <button ng-click="search()" class="btn btn-default search-project" type="button">חפש</button>
				            <button ng-click="clear()" ng-show="results" class="btn btn-default search-project" type="button">
					            <i class="fa fa-times"></i>
				            </button>
				        </span>
					</div>

					<div class="media" ng-if="results" ng-repeat="item in results">
						<div class="media-left">
							<a href="#">
								<img class="media-object" ng-src="{{itemImage(item)}}">
							</a>
						</div>
						<div class="media-body">
							<h4 class="media-heading">
								{{item.taxon.common_name.name}}
								<small>({{item.taxon.name}})</small>
								<button ng-click="save(item)" type="button" class="btn btn-success btn-sm pull-left">הוסף		</button>
							</h4>
							<p>
								{{item.user_login}}, <a target="_blank" href="{{item.uri}}">INAT</a>
								<br>
								{{item.updated_at | date:"dd/MM/yyyy"}}
							</p>
						</div>
					</div>

					<p ng-show="!results[0]">
						<span>אין תוצאות להציג</span>
					</p>
				</div>
			</div>
		</div>
	</div><!-- row -->
</div>

<script type="text/ng-template" id="confirmDelModal.html">
	<div class="modal-header">
		<h5 class="modal-title">
			<span>האם למחוק את</span>
			{{item.name}}
		</h5>
	</div>
	<div class="modal-footer tal">
		<button class="btn btn-default" type="button" ng-click="cancel()">ביטול</button>
		<button class="btn btn-primary" type="button" ng-click="ok()">מחק</button>
	</div>
</script>
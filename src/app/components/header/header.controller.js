(function() {
  'use strict';

  angular
    .module('golan')
    .controller('headerCtrl', headCtrl)
  ;

  function headCtrl($scope, $rootScope, Globals, $state, restApi, $window, AuthService, Notification, $timeout) {

    $scope.userMenuOpen = false;

    //  Scope Vars 
    $scope.activeAnimal = null;
    $scope.searchQuery = null;
    $scope.selectedProject = null;
    
    var data;
    var filters;
    var changeFilter = function () {
      $scope.advOpen = false;
      filters = {
        taxonId: angular.isObject($scope.activeAnimal) ? $scope.activeAnimal.id : null,
        query: $scope.searchQuery,
        project: $scope.selectedProject,
        advSearch: $scope.advSearch
      };

      $rootScope.$broadcast('headerFilter', filters);

      //  Send GA Event if this is adv Search
      if ($scope.advSearch && $window.ga) {
        ga('send', 'event', 'Advanced Search', 'Search');
      }

      //  If Were not in home page, go home and trigger search
      if (!$scope.showFilters) {
        $state.go('home', {filters: filters}).then(function () {
          $rootScope.$broadcast('headerFilter', filters);
        });
      }

    };
    var resetFilters = function () {
      $scope.searchQuery = null;  //  Query Filter (Search)
      $scope.activeAnimal = null; //  Animal Filter
    };
    resetFilters();

    $scope.changeFilter = changeFilter;
    $scope.filterAnimals = [
      {name: 'mammals', id: 40151, class: 'gicon-uni41', title: 'יונקים'},
      {name: 'birds', id: 3, class: 'gicon-uni43', title: 'עופות'},
      {name: 'rep', id: 26036, class: 'gicon-y', title: 'זוחלים'},
      {name: 'arachnida', id: 47119, class: 'gicon-m', title: 'עכבישים'},
      {name: 'insects', id: 47158, class: 'gicon-uni114', title: 'חרקים'},
      {name: 'plants', id: 47126, class: 'gicon-G2', title: 'צמחים'}
    ];
    
    //  Show Filter & search?
    $scope.showFilters = false;
    $scope.$on('ccActiveState', function (event, state) {
      $scope.showFilters = (state === 'home');
    });

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
 
      $scope.showFilters = (toState.name === 'home');
      $scope.advOpen = false;
      if (toState.name === 'home') {

        //  Check if state was change with params
        if(toParams.filter && toParams.filter.q){
          $scope.searchQuery = toParams.filter.q;
          $timeout( changeFilter, 500);
        }

        return false;
      }

      resetFilters();
    });
    

    $scope.filterAnimal = function (animal) {

      if (angular.isObject($scope.activeAnimal) && animal.id === $scope.activeAnimal.id) {
        animal = null;
      }else {
        //  Clear Advanced Search values
        $scope.taxaSearchTypeHead = animal.title;
      }
      
      $scope.activeAnimal = animal;


      //  Get Items
      changeFilter();
    };
    
    //  Allow Other controller to change the active project (Used in project ctrl)
    $scope.$on('activeProject', function () {
      $scope.selectedProject = $rootScope.activeProject;
    });

    $scope.clearSearch = function () {
      $scope.searchQuery = null;
      changeFilter();
    };
    
    var setGlobals = function () {

      //  Order Project - Default Project First!
      $scope.projOrder = function (project) {
        return data.defaultProjects.indexOf(parseInt(project.id)) * -1;
      };
      $scope.projects = data.projects;
    };

    $scope.$on('globals', function () {
      data = Globals.get();
      setGlobals();
    });

    data = Globals.get();
    if (angular.isDefined(data.projects)) {
      setGlobals();
    }


    //  Advanced Search Logic
    var today = new Date();
    $scope.advOpen = false;
    $scope.advSearch =  {
      active: false,
      dateRange: false,
      showEnd: false,
      showStart: false,
      startDate: new Date(today.getTime() - 90 * 24 * 60 * 60 * 1000),
      endDate: today,
      user: null
    };

    $scope.toggleDates = function (show) {
      var holder = !$scope.advSearch[show];
      $scope.advSearch.showEnd = false;
      $scope.advSearch.showStart = false;
      $scope.advSearch[show] = holder;
    };

    $scope.setTaxaTypeHead = function (item) {
      $scope.taxaSearchTypeHead = item.matched_term;
      $scope.activeAnimal = {id: item.id};
    };

    $scope.getTaxa = function (val) {
      return restApi.taxa.autocomplete(val)
        .then(function (results) {
          return results.results;
        });
    };

    //  Close Advanced search modal
    $scope.closeAdv = function(){
      $scope.advOpen = false;  
    };
    
    $scope.clearAdv = function () {
      $scope.advSearch.active = false;
      $scope.advSearch.user = null;
      $scope.activeAnimal = null;
      $scope.taxaSearchTypeHead = null;
      $scope.searchQuery = null;
      $scope.selectedProject = null;
      $scope.golbalObject.clear();
      changeFilter();
    };


    //  Object to connect autocompleate
    $scope.golbalObject = {};

    //  Send Events to google analytics (open/close advSearch)
    if ($window.ga) {
      var openClose,
          init = true;  //  Init var will prevent firing close event on page load 

      $scope.$watch('advOpen', function (data) {
        openClose = data ? 'open' : 'close';
        if(!init){
           ga('send', 'event', 'Advanced Search', openClose);
        }else {
          init = false;
        }

      });
    }


    /**
     *  Login Form Login
     */

    $scope.userInfo = {};
    $scope.userLoginData = {};
    $scope.loggedIn = false;
    $scope.loading = false;
    //  Send Function

    $scope.login = function () {

      //  Manualy make sure all fileds are checked
      angular.forEach($scope.loginForm.$error.required, function (field) {
        field.$setDirty();
      });

      //  All Ok, Send the fucker
      if ($scope.loginForm.$valid) {

        $scope.formDisabled = true;
        $scope.loading = true;

        AuthService.login($scope.userLoginData).then(
          function () {
            $scope.userLoginData.isopen = false;
            $scope.loading = false;
            Notification.success('התחברת בהצלחה');
          },
          function () {

            $scope.loading = false;
            $scope.loginForm.$setPristine();
            //  Remove inputs, keep dropdown open
            $scope.userLoginData = {
              isopen: true,
              error: 'כניסה נכשלה'
            };

          }
        );

      } else {
        $scope.loading = false;
      }
    };
    
    $scope.logout = function () {
      AuthService.logout();
    };

    $scope.$on('loginChanged', function (event, status) {
      //console.log('Login Changed',status);
      $scope.loggedIn = status;
      if (status) {
        AuthService.getUserInfo().then(function (data) {
          $scope.userInfo = data;
        });
      }
    });

  }

})();
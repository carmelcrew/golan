
<?php
App::uses('AppController', 'Controller');

class CuratorsController extends AppController {

    // public $components = array('Session');

    // Admin Functions

    public function control_index()
    {
        $this->headerMenu['curators'] = 'active';
        $this->Curator->recursive = -1;
        $all = $this->Curator->find('all');
        $this->set('items', $all);
        $this->set('title_for_layout', 'אוצרים');

        $t = $this->json_index(false);
        // pr($t);
    }

    //  Add Curator
    function control_add(){

        $this->layout = false;
        $data = $this->request->data;

        if ($this->request->is('post')) {
            $this->autoRender = false;

            //  Updating?
            if(isset($this->request->data['id']))
                $this->Curator->id = $this->request->data['id'];
            else //Creating
                $this->Curator->create();


            if($this->Curator->save($data)){
                return json_encode(array('success'=> true));
            }else {
                return json_encode(array('success'=> false));
            }
        }
    }

    //  Delete A Curator
    function control_del($id = null) {
        $this->autoRender = false;

        if($id) {
            $this->Curator->delete($id);
        }else{
            return 0;
        }

        return 1;
    }

    // JSON API functions

    // Curator index
    function json_index($encode = true)
    {
        $this->Curator->recursive = -1;
        $news = $this->Curator->find('all');
        // put them all in an anon array
        $ret = array();
        foreach( $news as $N )
            $ret[] = $encode ? $N['Curator'] : $N['Curator']['login'];
        // return json array of Curators
        return $encode ? json_encode($ret) : $ret;
    }
}

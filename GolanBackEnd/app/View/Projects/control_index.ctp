<?php
// pr($projects);
$intArr = array(
	'add'   			=> $this->Html->url('add'),
	'del'   			=> $this->Html->url('del'),
	'update'   			=> $this->Html->url('update'),
	'intSync'			=> $this->Html->url(array(
		"controller"=> "projects",
		"action" 	=> "sync",
		"control"	=> false,
		"json"		=> false
	), true),
	'projectsUrl'		=> $this->Html->url(array(
		"controller"=> "projects",
		"action" 	=> "index",
		"control"	=> false,
		"json"		=> true
	), true)
);
?>
<div class="projects" ng-controller="projectCtrl" ng-init='init(<?php echo json_encode($intArr); ?>)'>
	<div class="page-header">
		<h3>רשימת הפרוייקטים</h3>
		<span class="text-muted">פרוייקטים אלו יופיעו בניווט הראשי, וכן בפילטר בדף הראשי</span>
	</div>

	<div class="row">
		<div class="col-md-6">

			<div class="panel panel-primary cc-list">
				<div class="panel-heading">
					<span>רשימת הפרוייקטים</span>
					<i class="fa fa-spinner fa-spin" ng-show="loading"></i>
					<button class="btn btn-default btn-xs pull-left" ng-click="remoteRefresh()"  title="עדכן את נתוני הפרויקט לפי הגירסא ששמורה בשרת איינטורליסט">עדכן פרויקטים</button>
				</div>
				<table class="table table-striped list projects-list" ng-show="projects">
					<tr>
						<th>מספר</th>
						<th>שם הפרוייקט</th>
						<th>כתובת</th>
						<th>הגדרות</th>
						<th></th>
					</tr>
					<tr ng-repeat="project in projects">
						<td>{{project.id}}</td>
						<td>{{project.title}}</td>
						<td>{{project.slug}}</td>
						<td>
							<span title="להצי בתפריט הראשי"
							      class="pointer fa"
							      ng-class="project.menu_flag === '1' ? 'fa-eye' : 'fa-eye-slash'"
							      ng-click="changeFlag(project, 'menu_flag')">
							</span>

							<span class="pointer fa {{smartFlag[project.smart_flag].class}}"
							      title="{{smartFlag[project.smart_flag].title}}"
							      ng-click="changeFlag(project, 'smart_flag')">
							</span>
						</td>
						<td class="actions">
							<a href="#" ng-click="delete(project)"><span class="fa fa-remove"></span></a>
						</td>
					</tr>
				</table>
			</div>
		</div><!-- col-6 -->

		<!-- Add project -->

		<div class="col-md-6" ng-controller="AddProjectCtrl">

			<div class="panel panel-primary cc-list">

				<div class="panel-heading">הוספת פרוייקט חדש</div>

				<div class="panel-body">
					<label>חיפוש פרוייקט<i class="fa fa-spinner fa-spin" ng-show="loading"></i></label>

					<div class="input-group">
						<input type="text" ng-model="query" class="form-control" cc-enter="searchProject()" placeholder="הזן כתובת, שם או מזהה לפרוייקט">
				        <span class="input-group-btn">
				            <button ng-click="searchProject()" class="btn btn-default search-project" type="button">חפש</button>
				            <button ng-click="clear()" class="btn btn-default" type="button" ng-show="results">
								<i class="fa fa-times"></i>
							</button>
			            </span>
					</div>

					<div class="media" ng-if="results">
						<div class="media-left">
							<a href="#">
								<img class="media-object" ng-src="{{results.icon_url}}">
							</a>
						</div>
						<div class="media-body">
							<h4 class="media-heading">
								{{results.title}}
								<small>({{results.id}})</small>
								<button ng-click="saveProject(results)" type="button" class="btn btn-success btn-sm pull-left" ng-if="results">הוסף		</button>
							</h4>
							<p>{{results.description}}</p>
						</div>
					</div>

					<p ng-show="!results">
						<span>אין תוצאות להציג</span>
					</p>
				</div>
			</div>
		</div>
		<!-- / Add project -->

	</div><!-- row -->
</div>
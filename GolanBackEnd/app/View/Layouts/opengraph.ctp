<!doctype html>
<html>
<head>

    <title>תצפיטבע - קהילה מנטרת טבע בגולן</title>

    <?php
    /* old stuff
    $url = str_replace('/og','',$this->params->here);
    <meta property="og:url" content="<?php echo FULL_BASE_URL . $url; ?>" />
    <meta property="og:url" content="<?php echo FULL_BASE_URL . $this->params->here; ?>" />
    */
    ?>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="keywords" content="קהילה טבע ניטור גולן מועצה אזורית תצפיטבע אפליקציה חיות בגולן" />
    <meta property="og:locale" content="he_IL" />

    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="תצפיטבע" />
    <?php
    foreach ($meta as $key => $met)
        echo "<meta property=\"{$key}\" content=\"{$met}\" />\n";
    ?>

</head>
</html>
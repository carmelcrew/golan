'use strict';

angular.module('golanAdmin')

  // Directive to run function when ENTER key (13) is press
  .directive('ccEnter', function () {
    return function (scope, element, attrs) {
      element.bind('keydown, keypress', function (event) {
        if(event.which === 13) {
          scope.$apply(function (){
            scope.$eval(attrs.ccEnter);
          });

          event.preventDefault();
        }
      });
    };
  })

;
(function() {
  'use strict';

  angular
    .module('golan')
    .factory('AuthService', AuthService)
  ;

  function AuthService(Globals, $q, $http, $rootScope, config, Notification) {
    var authService = {};


    var userInfo    = {},
      isLoggedIn  = false,
      httpConfig  = { tokenRequired: true },
      globals     = Globals.get(),
      inatUrl     = config.apiUrl;

    $rootScope.$on('globals', function(){
      globals = Globals.get();
    });

    //  Do Login Post ACTION against cakephp
    var doLogin = function(credentials){

      var deferred = $q.defer();

      $http({
        method: 'POST',
        url: globals.backUrl + '/users/login',
        data: $.param(credentials),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ignoreLoadingBar: true
      })
        .then(function(results) {
          //console.log(results);
          if(results.data.success){
            deferred.resolve(results.data.access_token);
          }else {
            deferred.reject(results);
          }
        }, function(results){
          deferred.reject(results);
        });


      return deferred.promise;
    };

    var doLogout = function(){
      var deferred = $q.defer();

      globals.aToken = null;
      Globals.set(globals);

      //  Remove User Data
      userInfo = {};
      isLoggedIn = false;

      $rootScope.$broadcast('loginChanged', isLoggedIn);

      Notification.success('התנתקת מהמערכת בהצלחה');
      //  Let Cake know were loggin out
      $http.get(globals.backUrl + '/users/logout')
        .then(function() {
          deferred.resolve('Ok');
        });

      return deferred.promise;

    };

    var setInfoNotify = function(info){
      isLoggedIn  = true;

      getUserInfo(info).then(function(fixedInfo){
        $rootScope.$broadcast('loginChanged', isLoggedIn, fixedInfo);
      });
    };

    //  if data param is set, function will fix it and set it, if not, function will ask INAT for user info
    var getUserInfo = function(data){

      var deferred = $q.defer();

      var fixUserData = function(data){
        //  Get User First Name
        if(data.name){
          data.firstName = data.name.split(' ')[0];
        }else {
          data.firstName = data.login;
        }

        return data;
      };

      //  is data exsists?
      if(data){
        userInfo = fixUserData(data);
        deferred.resolve(userInfo);

      }else if(!angular.equals({}, userInfo)) {   //  User Info is saved allready, just return it

        deferred.resolve(userInfo);

      }else {

        $http.get(inatUrl + 'users/edit.json', httpConfig)
          .then(
            function (results) {
              userInfo = fixUserData(results.data);
              deferred.resolve(userInfo);

            },
            function (data) {
              console.log('CACACA', data);
              deferred.reject(data);
            }
          );

      }


      return deferred.promise;
    };

    //  Get Users project, update - Force Remote update
    var getUserProjects = function(update){
      //  This Function requiers user info to be set
      var deferred = $q.defer();
      if(!userInfo.login || !isLoggedIn){
        deferred.reject('No Login');
        return deferred.promise;
      }

      //  Did we get this users project allreay?
      if(angular.isDefined(userInfo.ccProjects) && !update){
        deferred.resolve(userInfo.ccProjects);
      }else {
        $http.get(inatUrl + 'projects/user/' + userInfo.login + '.json', httpConfig)
          .then(function (results) {
            userInfo.ccProjects = results.data;
            deferred.resolve(userInfo.ccProjects);
          }, function (data) {
            deferred.reject(data);
          });
      }

      return deferred.promise;

    };

    /**
     * Exposed Functions
     */

    authService.login = function (credentials) {
      var deferred = $q.defer();

      doLogin(credentials).then(
        function(accessToken){
          //  Add Token To globals
          globals.aToken = accessToken;
          Globals.set(globals);
          //TokenHolder.set(accessToken);
          //  Get User Info
          getUserInfo().then(
            function(data){
              //  Active, Set and Notify everybody
              setInfoNotify(data);
              deferred.resolve(data);
            },
            function(){
              deferred.reject('Some thing went wrong');
            }
          );
        },
        function(){
          deferred.reject();
        }
      );

      return deferred.promise;
    };

    //  Exposed Function to get user info,
    //  Return false if not connected
    authService.getUserInfo = function (){

      var deferred = $q.defer();
      if(!isLoggedIn){
        deferred.reject('Not Logged In');
        return deferred.promise;
      }

      getUserInfo().then(
        function(data){
          deferred.resolve(data);
        },
        function(data){
          deferred.reject(data);
        }
      );

      return deferred.promise;

    };

    authService.logout = function(){
      doLogout();
    };

    //  Test If user is singed to $project
    authService.userInProject = function(projectSlug){
      var deferred = $q.defer();

      //  First, Get All project
      getUserProjects().then(
        function(data){

          var projectFound = false;
          angular.forEach(data, function(item){

            if(item.project.slug === projectSlug){
              projectFound = true;
            }
          });
          deferred.resolve(projectFound);
        },
        function(data){
          console.log('Faild', data);
        }
      );
      return deferred.promise;
    };

    //  Get All User Projects
    authService.userAllProjects = function() {
      var deferred = $q.defer();

      //  First, Get All project
      getUserProjects(true).then(
        function (data) {
          deferred.resolve(data);
        },
        function (data) {
          deferred.reject(data);
        }
      );

      return deferred.promise;
    };

    //  Add User to project
    authService.addUserToProject = function(projectId){
      var deferred = $q.defer();
      $http.get(globals.backUrl + '/users/join_project/' + projectId, httpConfig).then(
        function(){
          getUserProjects(true);
          deferred.resolve();
        },
        function(){}
      );
      return deferred.promise;
    };

    //  Remove User From project
    authService.removeUserFromProject = function(projectId){
      var deferred = $q.defer();
      $http.get(globals.backUrl + '/users/leave_project/' + projectId, httpConfig).then(
        function(){
          getUserProjects(true);
          deferred.resolve();
        },
        function(){}
      );
      return deferred.promise;
    };

    //  Edit User Info
    authService.editUser = function(userLogin, fields){
      //console.log(fields);
      var deferred = $q.defer();

      var fd = new FormData();

      angular.forEach(fields, function(value, key){
        fd.append(key,value);
      });

      //  Post THIS
      $http.post(globals.backUrl + '/users/edit/' + userLogin, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined},
          tokenRequired: true,
          ignoreLoadingBar: true
        })
        .then(
          //  Acept INAT to return user info after edditing, if No id is set, something went wrong
          function(results){
            if(results.data.id) {
              userInfo = {};
              getUserInfo().then(function(){
                deferred.resolve();
              });

            }else {
              deferred.reject();
            }
          },
          function(){
            deferred.reject();
          }
        );
      return deferred.promise;
    };

    authService.onLoadStatus = function(){
      var deferred = $q.defer();

      //  Check If access token was set from cake
      if(globals.aToken){
        // Test if its active
        getUserInfo().then(
          function(data){
            //  Active, Set and Notify everybody
            setInfoNotify(data);
            deferred.resolve(data);
          },
          function(){
            //console.log('Filad', data);
            deferred.reject('Login Failed');
          }
        );
      }else {
        deferred.reject('No Access Token');
      }

      return deferred.promise;
    };

    //  Do Register Post against cakephp
    authService.registerUser = function(credentials){

      var deferred = $q.defer();

      $http({
        method: 'POST',
        url: globals.backUrl + '/users/register',
        data: $.param(credentials),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ignoreLoadingBar: true
      })
        .then(
          function(results) {

            //  console.log(results);
            if(results.data.success === true){
              //  We Have a success, do Front login
              var accessToken = results.data.login.access_token;
              //  Add Token To globals
              globals.aToken = accessToken;
              Globals.set(globals);

              //  Get User Info
              getUserInfo().then(
                function(data){
                  //  Active, Set and Notify everybody
                  setInfoNotify(data);
                  deferred.resolve(results.data);
                },
                function(){
                  //console.log(data);
                  deferred.reject();
                }
              );
            }else {
              deferred.reject(results.data);
            }

            return;

          },
          function(results){
            deferred.reject(results);
          }
        );


      return deferred.promise;
    };

    return authService;
  }
  
})();
(function() {
  'use strict';

  angular.module('golan')
    .filter('badDateToISO', badDateToISO)
  ;

    //  Filter MYSQL date stamp to Readble JS data,
    function badDateToISO() {
      return function (badTime) {
        var goodTime = badTime.replace(/(.+) (.+)/, "$1T$2Z");
        return goodTime;
      };
    }

})();
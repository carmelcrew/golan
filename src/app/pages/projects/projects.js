(function() {
  'use strict';

  angular.module('golan')
    .config(route)
    .controller('ProjectsCtrl', ctrl) //  Single Project Controller
    .controller('ProjectsIndexController', indexCtrl)
    .directive('projectList', list)
  ;


  var projectsUrl = 'app/pages/projects/';

  function route($stateProvider) {

    $stateProvider
      .state('projects.list', {
        url: '',
        controller: 'ProjectsIndexController',
        templateUrl: projectsUrl + 'index.html'
      })

      .state('projects.single', {
        url: '/view/:slug',
        templateUrl: projectsUrl + 'single.html',
        controller: 'ProjectsCtrl',
        resolve: {          
          //  If The slug parameter is an id, we need to transform it to slug string before rendering the page
          slug: function ($q, restApi, $stateParams) {

            var deferred = $q.defer();

            if(isNaN($stateParams.slug)) {
              deferred.resolve($stateParams.slug);
            }else {
              restApi.projects.getSlugById($stateParams.slug).then(function(data){
                deferred.resolve(data);
              })
            }

            
            return deferred.promise;

          }
        }
      })

      .state('projects.single.gallery', {
        parent: 'projects.single',
        url: '/:gallery',
        template: '<items-gallery items="items"></items-gallery>'
      })

      .state('projects.single.list', {
        parent: 'projects.single',
        url: '/:list',
        template: '<items-list items="items" class="no-bg two-lists"></items-list>'
      })

  }

  //  Single Project Controller
  function ctrl($scope, restApi, $rootScope, Globals, AuthService, userInfo, $timeout, slug) {

    //  console.log('Single Project: ', slug);

    var data = Globals.get();

    //  Global Scope Vars
    $scope.loadingItems   = true;   //  On loading indication
    $scope.q              = '';     //  Search Query Model
    $scope.project        = null;
    $scope.items          = [];
    $scope.mapOptions     = {
      zoom: 10,
      options: {
        scrollwheel: false
      }
    };
    $scope.itemsMarkers   = [];
    $scope.currentPage    = 1;
    $scope.userInProject  = false;
    $scope.projectMembers = null;

    $scope.pagOptions = {
      currentPage: 1,
      totalItems: 0,
      perPage: 30,
      pageChanged: function () {
        getItems();
      }
    };

    $scope.projectCashedInfo = null;

    //  Active Project Select has changed, Reload Page
    $scope.$on('activeProject', function () {
      //console.log($rootScope.activeProject);
      //$state.go('project', {slug: $rootScope.activeProject.slug});
    });

    //  Update Global Active Project according to Project url
    var updateGlobalProject = function(projectSlug){

      for (var i = 0, len = data.projects.length; i < len; i++) {
        if (data.projects[i].slug === projectSlug) {
          $rootScope.activeProject = data.projects[i];
          $scope.projectCashedInfo = data.projects[i];
          $rootScope.$broadcast('activeProject');
          break;
        }
      }
    };

    var getItems = function () {
      $scope.loadingItems = true;
      restApi.obs.project
        .get({
          project: slug,
          q: $scope.q,
          page: $scope.pagOptions.currentPage,
          perPage: $scope.pagOptions.perPage
        })
        .then(
          function (data) {

            $scope.loadingItems = false;
            $scope.items = data.items;
            $scope.pagOptions.totalItems = $scope.totalItems = data.total;


          },
          function (err) {
            console.log('Error: ', err);
          }
        );
    };

    var getProject = function () {

      updateGlobalProject(slug);

      restApi.projects.get({project: slug})
        .then(
          function (data) {

            $scope.project = data;

            getProjectMembers();

            //  Set Map Center
            if (angular.isDefined(data.latitude)) {
              $scope.mapOptions.center = {
                lat: data.latitude,
                lng: data.longitude
              };
            }
          },
          function (err) {
            console.log('Error', err);
          }
        );
      /*
       //  Members is disabled for now, requier auth to get it
       //  Get Project Memebers
       restApi.projects.members({
       project: $stateParams.name
       })
       .then(
       function(data) {
       console.log(data);
       }
       )

       */
    };
    
    var getProjectMembers = function() {

      restApi.projects.members({
        projectId : $scope.project.id,
        per_page  : 12
      })
        .then(function(data){
          $scope.projectMembers = {
            total : data.total_results,
            items : data.results
          }
        }, function (a,b,c) {
          console.log(a,b,c);
        });

    };

    //  User Click Search Btn
    $scope.search = function () {
      $scope.pagOptions.currentPage = 1;
      getItems();
    };

    $scope.clearSearch = function () {
      $scope.q = null;
      $scope.pagOptions.currentPage = 1;
      getItems();
    };

    getItems();
    getProject();

    /**
     * User-Project Issues
     */

    var doAuth = function (event, status) {
      $scope.loggedIn = status;

      if (status) {
        $scope.userLoggedIn = true;
        AuthService.userInProject($scope.project.slug).then(function (results) {
          $scope.userInProject = results;
        });

        //  Check If user is sing to this project
        AuthService.userInProject($scope.project.slug).then(function (data) {
          $scope.userInProject = data;
        });
      } else {
        $scope.userInProject = false;
        $scope.userLoggedIn = false;
      }
    };
    $timeout(function () {
      if (userInfo) {
        doAuth(null, true);
      }
    });

    $scope.userInProject = false;
    $scope.userLoggedIn = false;

    $scope.joinProject = function () {
      if (!angular.isDefined($scope.project.id)) {
        return false;
      }
      AuthService.addUserToProject($scope.project.id).then(
        function () {
          $scope.userInProject = true;
        },
        function () {
        }
      );
    };

    $scope.leaveProject = function () {
      if (!angular.isDefined($scope.project.id)) {
        return false;
      }
      AuthService.removeUserFromProject($scope.project.id).then(
        function () {
          $scope.userInProject = false;
        },
        function () {
        }
      );
    };

    $scope.$on('loginChanged', doAuth);
  }

  function indexCtrl($scope, userInfo, Globals, AuthService, $state) {

    var getUserProjects = function () {

      var globals = Globals.get(),
        userPorjectsIds = [];

      $scope.projects = globals.projects;


      //  Get This User projects, If logged in
      AuthService.userAllProjects()
        .then(
          function (data) {
            $scope.userProjects = data;

            //  Create an ids array for filtering
            userPorjectsIds = [];
            angular.forEach(data, function (project) {
              this.push(project.project_id);
              project.project.ccUserSingTo = true;
            }, userPorjectsIds);


            //  Filter the users projects out from the available projects list
            var tempProjects = [];
            angular.forEach($scope.projects, function (proj) {
              //  Return only if the id of the project wasnt found at userProjectsIds array
              if (userPorjectsIds.indexOf(parseInt(proj.id)) === -1) {
                proj.ccUserSingTo = false;
                proj.ccUserLoggedIn = true;
                this.push(proj);
              }
            }, tempProjects);

            $scope.projects = tempProjects;

          },
          function () {
            //  User Not logged in
            $scope.userProjects = false;
            angular.forEach(globals.projects, function (proj) {
              proj.ccUserSingTo = false;
              proj.ccUserLoggedIn = false;
            })
            ;
          }
        );
    };

    $scope.showTitle = true;
    if ($state.current.url === "/projects") {
      $scope.showTitle = false;
    }

    $scope.userProjects = null;

    getUserProjects();

    $scope.$on('refreshProjects', function () {
      getUserProjects();
    });

    $scope.$on('loginChanged', getUserProjects);

  }

  function list(AuthService, Notification, $window, Globals, $uibModal) {
    var link = function ($scope, $element, $attrs) {

      $scope.noProjNotice = $attrs.errorNotice;
      var defualtProjects = Globals.get().defaultProjects,
        cashedProjects = Globals.get().projects,
        checkIfProjectCashed = function (projectId) {
          var temp = null;
          angular.forEach(cashedProjects, function (proj) {
            if (parseInt(proj.id) === projectId) {
              temp = proj;
            }
          });

          return temp;
        };

      //  Hide Actions
      $scope.showActions = function (project) {
        return defualtProjects.indexOf(parseInt(project.id)) === -1;
      };

      $scope.list = [];

      var init = function (newVal) {

        $scope.list = newVal;

        //  Fix Data from inat, Inat
        if ($scope.list[0] && $scope.list[0].project) {
          var tempProjects = [];
          angular.forEach($scope.list, function (project) {


            //  Search Project information at our data source projects
            var pr = checkIfProjectCashed(project.project_id);

            if (pr) {
              project.project.observations_count = pr.observations_count;
              project.project.taxa_count = pr.taxa_count;
            }

            this.push(project.project);
          }, tempProjects);
          $scope.list = tempProjects;
        }
      };

      $scope.projOrder = function (project) {
        return defualtProjects.indexOf(parseInt(project.id)) * -1;
      };


      //  Join A Project
      $scope.join = function (project) {
        AuthService.addUserToProject(project.id).then(function () {
          Notification.success('הצטרפת לפרויקט בהצלחה');
          $scope.$emit('refreshProjects');
        });
      };

      //  Leave A Project
      $scope.leave = function (project) {


        var modalInstance = $uibModal.open({
          templateUrl: 'leaveProjectModal.html',
          controller: function ($scope, $uibModalInstance, project) {
            $scope.project = project;

            $scope.ok = function () {
              $uibModalInstance.close();
            };

            $scope.cancel = function () {
              $uibModalInstance.dismiss();
            };
          },
          size: 'sm',
          resolve: {
            project: project
          }
        });

        modalInstance.result.then(function () {
          AuthService.removeUserFromProject(project.id).then(function () {
            Notification.primary('עזבת את הפרויקט');
            $scope.$emit('refreshProjects');
          });
        });
      };

      $scope.$watch('projects', init, true);

    };

    return {
      restrict: 'E',
      scope: {
        'projects': '='
      },
      link: link,
      templateUrl: projectsUrl + 'partials/list.html'
    }
  }

})();
(function() {

  'use strict';

  angular.module('golan')
    .controller('ActivityController', ctrl)
    ;

  function ctrl($scope, restApi, Globals, $state) {

    var globals = Globals.get();

    //  Get intro Text
    $scope.text = '';
    restApi.cake.texts('activity').then(function(text){
      $scope.text = text;
    });

    //  Date to filter by are set by backend
    var dates = globals.dates,
        allowedFilters = {
          week  : {
            d1 : dates.week,
            d2 : dates.today
          },
          month : {
            d1 : dates.month,
            d2 : dates.today
          },
          year  : {
            d1 : dates.year,
            d2 : dates.today
          },
          ever  : {}
        };


    //  Order Project - Default Project First!
    var defaultProjectIndex = 0;
    $scope.projOrder = function(project){
      return globals.defaultProjects.indexOf(parseInt(project.id)) *-1;
    };

    $scope.projects = globals.projects;
    $scope.activeProject = $scope.projects[defaultProjectIndex];
    angular.forEach($scope.projects, function(project){

      if(globals.defaultProjects.indexOf(parseInt(project.id)) !== -1){
        $scope.activeProject = project;
      }

    });

    $scope.items = null;        //  Top Observed Spieces
    $scope.observers = null;    //  Top Observers users
    $scope.identifiers = null;  //  Top Identifiers Users

    $scope.filterBy = 'month';

    $scope.filter = function(filter){
      $scope.filterBy = filter;
      getItems(allowedFilters[filter]);
      getUsers(allowedFilters[filter]);
    };

    $scope.changeProject = function(project){

      $scope.activeProject = project;

      getItems(allowedFilters[$scope.filterBy]);
      getUsers(allowedFilters[$scope.filterBy]);
    };

    $scope.taxaLink = function(item){
      $state.go('home', {filter : {q: item.taxon.preferred_common_name}});
    };

    var getItems = function(query){
      if($scope.activeProject.slug){
        query.project_id = $scope.activeProject.slug;
      }

      restApi.stats.mostViewed(query).then(function(data) {
        $scope.items = data.results;
      });
    };
    
    var getUsers = function(query){

      if($scope.activeProject.slug){
        query.project_id = $scope.activeProject.slug;
      }

      //  Get Top Observers
      restApi.stats.topObservers(query).then(function(data){
        $scope.observers = data.results;
      });
  
      //  Get Top Identifiers
      restApi.stats.topIdentifiers(query).then(function(data){
        $scope.identifiers = data.results;        
      });
    };    

    //  Start With "month" filter...
    getItems(allowedFilters['month']);
    getUsers(allowedFilters['month']);
    
  }

})();
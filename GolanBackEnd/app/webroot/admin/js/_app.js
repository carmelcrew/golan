'use strict';

angular.module('golanAdmin', ['ui.bootstrap', 'dndLists', 'ui-notification', 'textAngular'])

  .config(function( $provide,NotificationProvider) {
    NotificationProvider.setOptions({
      delay: 3000,
      startTop: 20,
      startRight: 0,
      verticalSpacing: 20,
      horizontalSpacing: 20,
      positionX: 'left',
      positionY: 'top'
    });
    
   $provide.decorator('taOptions', function($delegate) {

       $delegate.toolbar = [
          ['bold', 'italics', 'underline', 'insertLink']
        ];
        return $delegate;
    });
  })

  .controller('appCtrl', function($scope) {    })

  /**
   *  Projects Logics
   */
  .controller('projectCtrl', function($scope, $http, Notification){

    var update = function(project){

      $scope.loading = true;

      $http({
        method  : "POST",
        headers : {'Content-Type': 'application/x-www-form-urlencoded'},
        url     : $scope.initData.update,
        data    : $.param(project)
      })
        .then(function(data){
          $scope.loading = false;
          if(data.data.success){
            Notification.success('הפרויקט עודכן');
            getProjects();
          }else {
            Notification.error('שגיאה בעדכון הפרוייקט');

          }
        });
    };

    var getProjects = function(){
      $scope.loading  = true;
      $http.get($scope.initData.projectsUrl).then(function(data){
        $scope.projects = data.data;
        $scope.loading  = false;
      });
    };

    $scope.initData = null;
    $scope.loading  = true;

    $scope.smartFlag = {
      "0" :{
        class: 'fa-square-o',
        title: 'לא מסומן'
      },
      "1" :{
        class: 'fa-check-square',
        title: 'מסומן'
      },
      "2" :{
        class: 'fa-lock',
        title: 'נעול'
      }
    };

    //  Flag Changing function
    $scope.changeFlag = function(project, flag){
      //console.log(project, flag);

      //  Do Auto Toggle
      var val  = parseInt(project[flag]),
          newVal;
      if(flag === 'menu_flag'){
        newVal = +!val; //  Bool to int
      }else if(flag === 'smart_flag'){
        newVal = val+ 1;
        if(newVal === 3){
          newVal = 0;
        }
      }

      project[flag] = newVal;
      update(project);

    };


    //  Fire on load, get some reference data
    $scope.init = function(info){
      //console.log(info);
      $scope.initData = info;
      getProjects();
    };

    $scope.delete = function(project){
      //console.log(project);
      $scope.loading = true;
      $http.get($scope.initData.del + '/' + project.id).then(function(){
        Notification.success('פרוייקט הוסר בהצלחה');
        getProjects();
      });
    };

    $scope.$on('itemsChanged', function(){
      getProjects();
    });

    $scope.remoteRefresh = function(){
      $scope.loading = true;

      $http.get($scope.initData.intSync).then(function(d){
        $scope.loading = false;
        getProjects();
      });
    };

  })

  .controller('AddProjectCtrl', function($scope, $http, $q, Notification){

    var urlFixAndValid = function(query){

          //  Is empty?
          if(!query){
            return false;
          }

          //  Is Url?
          if($scope.query.indexOf("http") > -1){
            query = query.split("/").slice(-1);
          }


          return 'http://www.inaturalist.org/projects/'+ query +'.json';

        },
        search = function(queryText){

          var deferred = $q.defer();
          var url = urlFixAndValid(queryText);

          if(url) {

            $http({
              method: 'GET',
              url: url,
            })
              .then(
                function (data) {
                  deferred.resolve(data.data);
                },
                function (data) {
                  deferred.reject('error');
                }
              )
            ;
          }else {
            deferred.reject('invalid Query');
          }
          return deferred.promise;
        };

    $scope.query = null;
    $scope.loading = false;
    $scope.results = null;

    $scope.searchProject = function(){
      $scope.loading = true;

      search($scope.query).then(
        function(data){
          $scope.loading = false;
          $scope.results = data;
        },
        function(data){
          console.log(data);
          $scope.loading = false;
          $scope.results = null;
        }
      );
    };

    $scope.saveProject = function(project) {

      $scope.loading = true;
      var params = {
        id:                   project.id,
        title:                project.title,
        slug:                 project.slug,
        description:          project.description,
        'icon_url':           project.icon_url,
        'observations_count': project.project_observations_count,
        'taxa_count':         project.observed_taxa_count
      };

      $http({
        method  : "POST",
        headers : {'Content-Type': 'application/x-www-form-urlencoded'},
        url     : $scope.$parent.initData.add,
        data    : $.param(params)
      })
        .then(function(data){
          $scope.loading = false;
          if(data.data.success){
            Notification.success('הפרוייקט נוסף בהצלחה');

            // Update Parent Ctrl
            $scope.$emit('itemsChanged');
          }else {
            Notification.error('שגיאה בהוספת הפרוייקט');

          }
        });
    };

    $scope.clear = function(){
      $scope.results = null;
    };

  })

  /**
   * Pics Logics
   */
  .controller('picsCtrl', function($scope, $http, $window, $uibModal, Notification){
    $scope.items = [];
    $scope.loading = false;
    var initData,
        getItems    = function(){
            $scope.loading = true;
            $http.get(initData.picsJson).then(function(data){
                $scope.items = data.data;
                $scope.loading = false;
            });
        },
        saveOrder   = function(){
            $scope.loading = true;
            $http({
                url: initData.reOrder,
                method: 'POST',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: $.param({
                    items: $scope.items
                })
            }).then(
              function(results){

                  if(results.data == 1){
                      $scope.loading = false;
                  }else {
                      alert('תקלה בשמירה');
                  }
              },
              function(data){console.log(data);}
            );
        };

    $scope.init = function(data){
        $scope.backUrls = initData = data;
        //console.log(initData);
        getItems();
    };
    // Model to JSON for demo purpose
    $scope.$watch('items', function(model) {
        $scope.modelAsJson = angular.toJson($scope.items, true);
    }, true);

    $scope.reOrder = function(index){

        $scope.items.splice(index, 1);

        angular.forEach($scope.items, function(value, $index) {
            value.order = $index;
        });

        saveOrder();
    };

    $scope.del = function(item){
      //$window.location = initData.del + '/' + item.id;
      var modalInstance = $uibModal.open({
        templateUrl: 'confirmDelModal.html',
        controller: 'confirmDelModalCtrl',
        size: 'sm',
        resolve: {
          item: function () {
            return item;
          }
        }
      });

      modalInstance.result.then(
        function () {
          //  Delete Approved, do it man
          $scope.loading = true;

          $http.get(initData.del + '/' + item.id).then(function(){
            $scope.loading = true;
            Notification.success('הפריט נמחק בהצלחה');
            getItems();
          });
        }, function () {
          //console.log('Dissmised');
        }
      );
    };

    $scope.edit = function(item){

            var modalInstance = $uibModal.open({
                templateUrl: initData.edit + '/' + item.id,
                controller: 'EditPicModalCtrl',
                size: 'md',
                windowClass: 'dasasd',
                resolve: {
                    data: function () {
                        return {
                            item: item,
                            url : initData.edit + '/' + item.id
                        };
                    }
                }
            });
        };

    //  List to items change event from other Controllers
    $scope.$on('itemsChanged', function() {getItems();});

    })

  //  Confirm Delete Pic Modal
  .controller('confirmDelModalCtrl', function ($scope, $uibModalInstance, item) {

    $scope.item = item;

    $scope.ok = function () {$uibModalInstance.close();};

    $scope.cancel = function () { $uibModalInstance.dismiss(); };
  })


  .controller('AddPicCtrl', function($scope, $http, $q, Notification){

    var url = 'http://www.inaturalist.org/observations.json',
        singleObUrl = 'http://www.inaturalist.org/observations/',

    search = function(queryText){

      var deferred = $q.defer();

      $http({
        method: 'GET',
        url: isNaN(queryText) ? url : singleObUrl + '/' + queryText + '.json',
        params: {
          q:          queryText,
          projects:   'tatzpiteva',
          locale:     'iw',
          per_page:   10
        }
      })
        .then(
          function (data) {
            deferred.resolve(data.data);
          },
          function (data) {
            deferred.reject('error');
          }
        )
      ;

      return deferred.promise;
    };

    $scope.query = null;
    $scope.query = '';
    $scope.loading = false;
    $scope.results = null;

    $scope.itemImage = function(item){

          if(item.photos && item.photos.length){
              return item.photos[0].medium_url;
          }else if(item.observation_photos && item.observation_photos.length) { //  Item from single request
              return item.observation_photos[0].photo.medium_url;
          }

          return null;
      };

    $scope.search = function(){
      $scope.loading = true;

      search($scope.query).then(
        function(data){
          $scope.loading = false;
          $scope.results = angular.isArray(data) ? data : [data];
        },
        function(data){
          console.log(data);
          $scope.loading = false;
          $scope.results = null;
        }
      );
    };

    $scope.save = function(item) {

      $scope.loading = true;
      item.ccImgUrl = $scope.itemImage(item);

      $http({
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        url: $scope.$parent.backUrls.add,
        data: $.param(item)
      })
        .then(function(data){
          $scope.loading = false;
          if(data.data.success){
            Notification.success('נשמר בהצלחה');

            // Update Parent Ctrl
            $scope.$emit('itemsChanged');
          }else {
            Notification.error('השמירה נכשלה');
          }
        });
    };

    $scope.clear = function(){
      $scope.results = null;
    }

  })

  .directive('ccModal', function( $uibModal){
        return {
            restrict: 'CA',
            replace: false,
            transclude: false,
            link: function(scope, elem, attrs) {

                elem.bind('click', function(event) {
                    event.preventDefault();
                    var modalInstance = $uibModal.open({
                        templateUrl: attrs.href,
                        controller: attrs.ccModal,
                        size: 'md'
                    });

                    modalInstance.result.then(function () {
                        console.log('Close 1');
                    }, function () {
                        console.log('Close 2');
                    });
                });
            }
        }
  })

  .controller('EditPicModalCtrl', function($scope, $http, $q, $window, $uibModalInstance, data){
        var initInfo,
            url = 'http://www.inaturalist.org/observations/',
            getInatIobject = function () {
                $http({
                    method: 'GET',
                    url: url + $scope.item.id + '.json'
                })
                .then(
                    function (data) {
                        $scope.inatItem = data.data;
                        $scope.loading = false;
                    }
                )
            };

        $scope.loading = true;
        $scope.item = data.item;
        $scope.inatItem = null;
        $scope.selectedImg = 0;

        $scope.changeImg = function(index){
            $scope.selectedImg = index;
        };

        $scope.save = function(){
            $scope.loading = true;
            var itemData = {
                url : $scope.inatItem.observation_photos[$scope.selectedImg].photo.medium_url,
                name: $scope.item.name,
                id  : $scope.item.id
            };

            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

            $http({
                url: data.url,
                method: 'POST',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: $.param({
                    item: itemData,
                    action: 'save'
                })
            }).then(
              function(data){
                  $scope.loading = false;
                  $uibModalInstance.close('Changeed');
              },
              function(data){}
            )
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        //  Lets run
        getInatIobject();


    })

;

//  Register Filters
angular.module('golanAdmin')
  //  Filter MYSQL date stamp to Readble JS data,
  .filter('badDateToISO', function() {
    return function(badTime) {
      var goodTime = badTime.replace(/(.+) (.+)/, "$1T$2.000Z");
      return goodTime;
    };
  })
  .filter('simpleMysql', function() {
    return function(mysqlDate) {
      //var goodTime = mysqlDate.replace(/(.+) (.+)/, "$1T$2.000Z");
      return new Date(mysqlDate);
    };
  })
;

(function() {
  'use strict';


  angular.module('golan')
    .controller('ArticlesController', ctrl)
  ;


  function ctrl($scope, restApi, Globals) {

    $scope.webRoot = Globals.get().webroot;
    $scope.items = null;
    $scope.cats  = null;
    $scope.activeCat = null;

    var getItems = function(){
      restApi.cake.articles()
        .then(
        function(data) {
          $scope.items = data.items;
          $scope.cats = data.cats;
        },
        function(){}
      );
    };

    $scope.setActiveCat = function(cat){
        //if(cat)
        if($scope.activeCat && $scope.cats[cat].categories.id === $scope.activeCat.categories.id){
            $scope.activeCat = null;
        }else{
            $scope.activeCat = $scope.cats[cat];
        }
    };

    getItems();

  }

})();